/*----- CREACIÓN DE LOS OBJETOS*/

var Grupo = function(
  _clave,
  _nombre,
  _docente,
  _materias,
  _alumnos){
      return{
          "clave":_clave,
          "nombre":_nombre,
          "docente":_docente,
          "materias":_materias,
          "alumnos":_alumnos
      };
};

var Docente = function (
    _clave,
    _nombre,
    _apellidos,
    _grado_academico){
        return{
            "clave":_clave,
            "nombre":_nombre,
            "apellidos":_apellidos,
            "grado_academico":_grado_academico
        };
    };

var Materia = function(
    _clave,
    _nombre){
        return{
            "clave":_clave,
            "nombre":_nombre
        };
};

var Alumno = function(
    _clave,
    _nombre,
    _apellidos,
    _calificaciones){
        return{
            "clave":_clave,
            "nombre":_nombre,
            "apellidos":_apellidos,
            "calificaciones":_calificaciones
        };
};

/*--------- CREACIÓN DE AS FUNCIONES PARA CADA OBJETO ---------*/

// -------- FUNCIONES DE DOCENTE -------------

function asignarDocente(g,d){
    if(!g.docente){
        g.docente = d;
        return true;
    }
    return false;
}

function asignarDocente(g,d){
    if(!g.docente){
        g.docente = d;
        return {"exito":true,
        "mensaje":"Docente asignado al grupo " + g.nombre
    };}
    return {
        "exito":false,
        "mensaje":"El grupo " + g.nombre + " ya tiene docente asignado"
    };
}

function docenteAsignado(g,cd){
    if(g.docente.clave==cd){
        return true;
    }
    return false;
}


// -------- FUNCIONES DE GRUPO -------------

function setNombreGrupo(g,n){
    if(!g.nombre){
        g.nombre = n;
        return true;
    }
    return false;
}

function asignarNombre(g,n){
    if(!g.nombre){
        g.nombre = n;
        return true;
    }
    return false;
}


// -------- FUNCIONES DE MATERIA -------------


function asignarMateriaAlGrupo(g,m){
    if(!g.materias){
        g.materias = [];
    }
    if(!existeMateriaEnGrupo(g, m.clave)){
        g.materias.push(m);
        return {"mensaje":"Se agregó correctamente la materia"};
    }
    return {"mensaje":"Ya existe la materia"};
}

function existeMateriaEnGrupo(g,cm){
    if(!g.materias || g.materias.length === 0){
        return {"mensaje":"No existe la materia"};
    }
    for(var i=0; i<g.materias.length; i++){
        if(g.materias[i].clave === cm){
            return {"mensaje":"Ya existe la materia"};
        }
    }
    return {"mensaje":"No existe la materia"};
}

function buscarMateria(g,cm){
    for(var i = 0; i<g.materias.length; i++){
        if(g.materias[i].clave===cm){
       return i;
     }
   }
   return -1;
}



 function eliminarMateria(g,cm){
   var posicion = buscarMateria(g,cm);
   if(posicion!=-1){
     g.materias.splice(posicion,1);
   }
   else{
     return{"mensaje":"No existe la materia"};
   }
 }


// -------- FUNCIONES DE ALUMNO -------------

function asignarAlumnoAlGrupo(g,a){
    if(!g.alumnos){
        g.alumnos = [];
    }
    if(!existeAlumnoEnGrupo(g, a.clave)){
        g.alumnos.push(a);
        return {"mensaje":"Se agregó correctamente al alumno"};
    }
    return {"mensaje":"Ya está inscrito este alumno en este grupo"};
}

function existeAlumnoEnGrupo(g,ca){
    if(!g.alumnos || g.alumnos.length === 0){
        return {"mensaje":"No existe ningún alumno"};
    }
    for(var i=0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave === ca){
            return {"mensaje":"Ya existe el alumno"};
        }
    }
    return {"mensaje":"No existe el alumno"};
}

function buscarAlumno(g,ca){
    for(var i = 0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave===ca){
       return i;
     }
   }
   return -1;
}



 function eliminarAlumno(g,cm){
   var posicion = buscarAlumno(g,cm);
   if(posicion!=-1){
     g.materias.splice(posicion,1);
   }
   else{
     return{"mensaje":"No existe el alumno"};
   }
 }

// -------- UTILIZACIÓN DE LAS FUNCIONES -------------

var grupo1 = Grupo('001', 'ISA');
var grupo2 = Grupo('002', 'ISB');

var materia1 = Materia('M01','Bases de datos');
var materia2 = Materia('M02','Programación');
var materia3 = Materia('M03','Programación Web');
var materia4 = Materia('M04','Estructura de datos');
var materia5 = Materia('M05','Redes');

var docente1 = Docente('D20','Antonio','Hernández Blas');
var docente2 = Docente('D30','Jorge','Santos Santos');

var alumno1= Alumno('09161299','Noé Faustino','Silva Juárez','80 80 80 70 85');
var alumno2= Alumno('09152230','Juan','Pérez','90 79 85 90 90');
var alumno3= Alumno('09152960','Pablo','Zárate Zárate','90 79 85 90 90');

asignarMateriaAlGrupo(grupo1, materia1);
asignarMateriaAlGrupo(grupo1, materia2);
asignarMateriaAlGrupo(grupo1, materia3);
asignarMateriaAlGrupo(grupo1, materia4);
asignarMateriaAlGrupo(grupo1, materia5);

asignarAlumnoAlGrupo(grupo1,alumno1);
asignarAlumnoAlGrupo(grupo1,alumno2);
asignarAlumnoAlGrupo(grupo1,alumno3);

